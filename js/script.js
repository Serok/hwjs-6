// ВОПРОС:
// Опишите своими словами как работает цикл forEach.
// ОТВЕТ:
// forEach - это способ перебора элементов массива, которому может быть присвоена функция,
// аргументом которой будет являться элемент этой функции. Функция выполниться по одному разу для каждого элемента массива.

// ЗАДАНИЕ:
// Реализовать функцию фильтра массива по указанному типу данных.
//     Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     Написать функцию filterBy(), которая будет принимать в себя 2 аргумента.
//     Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
//     Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент,
//     за исключением тех, тип которых был передан вторым аргументом.
//     То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string',
//     то функция вернет массив [23, null].

function filterBy(array, type) {
    return array.reduce((newArray, item) => {
        if (typeof item != type) {
            newArray.push(item);
        }
        return newArray
    }, []);
}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'))